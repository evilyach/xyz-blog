// Select Document Object Module (DOM) Items
const menu = document.querySelector('.menu');
const menu_btn = document.querySelector('.menu-btn');
const menu_branding = document.querySelector('.menu-branding');
const menu_nav = document.querySelector('.menu-nav');
const nav_items = document.querySelectorAll('.nav-item');

// Set initial state of the Menu
var show_menu = false;
menu_btn.addEventListener('click', toggleMenu);

function toggleMenu() {
    if (!show_menu) {
        menu.classList.add('show');
        menu_btn.classList.add('close');
        menu_nav.classList.add('show');
        menu_branding.classList.add('show');
        nav_items.forEach(item => item.classList.add('show'));

        show_menu = true;
    } else {
        menu.classList.remove('show');
        menu_btn.classList.remove('close');
        menu_nav.classList.remove('show');
        menu_branding.classList.remove('show');
        nav_items.forEach(item => item.classList.remove('show'));

        show_menu = false;
    }
}